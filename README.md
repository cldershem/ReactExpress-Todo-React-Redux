# React Basic Outline
## [Pages URL](https://cldershem.gitlab.io/ReactExpress-Todo-React-Redux/)
## Install
```sh
git clone git@gitlab.com:cldershem/ReactExpress-Todo-React-Redux.git
cd ReactExpress-Todo-React-Redux
npm install
```

## Run
`npm run dev`

## Build
`npm run build`

## Notes
- `build` runs `webpack` and `babel` to compile all your JS to a file, `/dist/bundle.js`
- `bundle.js` is all of your JS in one file including all libraries used
- `index.html` loads './dist/bundle.js'
- because of this your site can now be 'static' which means you can use something like github or gitlab pages
- `.gitlab-ci.yml` is kind a basic example of how to do this on gitlab pages
