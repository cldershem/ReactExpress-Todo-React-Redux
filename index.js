import React, { Component } from 'react';
import { render } from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import { reducer } from './actions/todoListRedux';
const store = createStore(reducer);

import App from './components/App';

const AppWithStore = (
  <Provider store={store}>
    <App />
  </Provider>
);

render(AppWithStore, document.querySelector('#app'));
